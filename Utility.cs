﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionOpen
{
	internal abstract class Utility
	{
		public static string GetFileBaseName(string filePath)
		{
			int fileNameStart = filePath.LastIndexOfAny("\\/".ToCharArray()) + 1;

			if(fileNameStart > 0)
			{
				return filePath.Substring(fileNameStart, (filePath.Length - fileNameStart));
			}

			return filePath;
		}

		public static string[] SplitExtension(string filePath)
		{
			string[] output = new string[2] { string.Empty, string.Empty };

			if(filePath.Length > 0)
			{
				int fileNameStart = filePath.LastIndexOfAny("\\/".ToCharArray()) + 1;
				int firstDot = filePath.IndexOf('.', fileNameStart);

				if(firstDot == 0)
				{
					// Ignore the first dot if it happens to come at the very beginning of the path.
					// This is consistent with Python's behavior in `os.path.splitext()`.
					firstDot = filePath.IndexOf('.', firstDot + 1);
				}

				if(firstDot > 0)
				{
					output[0] = filePath.Substring(0, firstDot);
					output[1] = filePath.Substring(firstDot, filePath.Length - firstDot);
				}
			}

			return output;
		}

		public static string[] GetValidFileExtensionsForHeaderFlip()
		{
			return new string[]
			{
				".asm",
				".c",
				".cc",
				".cp",
				".cpp",
				".cxx",
				".c++",
				".h",
				".hh",
				".hpp",
				".h++",
				".inl",
				".m",
				".mm",
				".s",
			};
		}

		public static string[] GetForbiddenFileExtensions()
		{
			return new string[]
			{
				".csproj",
				".csproj.user",
				".fsproj",
				".fsproj.user",
				".jsproj",
				".jsproj.user",
				".sln",
				".vbproj",
				".vbproj.user",
				".vcxproj",
				".vcxproj.filters",
				".vcxproj.user",
				".wixproj",
				".wixproj.user",
			};
		}
	}
}
