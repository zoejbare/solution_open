
Version 1.1.0 (7/3/2020):
* See https://gitlab.com/zoejbare/solution_open/-/releases for release notes of all future releases starting at version 1.1.0.

Version 1.0.2 (2/8/2018):
* Added more file types to the list of valid extensions (.cfg, .config, .cs, .css, .htm, .html, .ini, .json, .res, .resx, .txt, .vsct, .vsixmanifest).

Version 1.0.1 (9/20/2015):
* Added new extensions to the header flip (.m, .mm, .py, .asm, .s, .def).

Version 1.0 (8/9/2015):
* Ported original code to an extension for Visual Studio 2012-2015.
* Restored build timing code.
* Added events to automatically refresh the file list.
* Fixed the display limit in the dialog window from incorrectly clearing the list of results.
* Guarded against a potential unhandled exception when parsing the regex expressions in the dialog window.
