﻿using EnvDTE;
using Microsoft;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Debugger.Interop;
using Microsoft.VisualStudio.OLE.Interop;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.Text;
using Microsoft.VisualStudio.Threading;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Task = System.Threading.Tasks.Task;

namespace SolutionOpen
{
	public class PackageData
	{
		public PackageData(DTE dte, IVsOutputWindowPane debugInfowWindowPane)
		{
			Assumes.Null(ms_instance);
			ms_instance = this;

			m_dte = dte;
			m_debugInfoWindowPane = debugInfowWindowPane;
		}

		#region Static Members

		public static PackageData Instance { get { return ms_instance; } }

		private static PackageData ms_instance;

		#endregion
		#region Instance Members

		public StringCollection SolutionFiles
		{
			get
			{
				if(m_dirty)
				{
					// Normally, it would be possible to use a CommonMessagePump here to display
					// the VS wait dialog if the operation takes too long, but the issue we run
					// into is that several of the things we access here like to be accessed from
					// only the UI thread, which is where the wait dialog also runs.  That means
					// switching to the main thread makes the dialog useless since it would be
					// blocked from running until all the operations here complete anyway.

					ThreadHelper.JoinableTaskFactory.Run(async () => {
						await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

						ICollection<string> filePaths = new HashSet<string>();
						Projects projects = m_dte.Solution.Projects;

						foreach (Project project in projects)
						{
							RecurseProjectItems(project.ProjectItems, ref filePaths);
						}

						m_solutionFiles.Clear();
						m_solutionFiles.AddRange(filePaths.ToArray());
					});

					m_dirty = false;
				}

				return m_solutionFiles;
			}
		}

		private DTE m_dte;
		private IVsOutputWindowPane m_debugInfoWindowPane;

		private StringCollection m_solutionFiles = new StringCollection();
		private bool m_dirty = true;

		#endregion
		#region Methods

		public void MakeDirty()
		{
			m_dirty = true;
		}

		public void OpenFiles(string[] filePaths)
		{
			ThreadHelper.JoinableTaskFactory.Run(async () => {
				await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

				foreach(string path in filePaths)
				{
					try
					{
						m_dte.ItemOperations.OpenFile(path);
					}
					catch(Exception e)
					{
						OutputDebugInfo($"Failed to open file: {path}\n\t{e.Message}\n");
					}
				}
			});
		}

		public void HandleHeaderFlip()
		{
			ThreadHelper.JoinableTaskFactory.Run(async () => {
				await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

				// Make sure there is an active document.
				if(m_dte.ActiveDocument != null)
				{
					string[] validFileExtensions = Utility.GetValidFileExtensionsForHeaderFlip();

					// Get the current active document and its base name.
					string documentFullPath = m_dte.ActiveDocument.FullName;
					string documentBaseName = Utility.SplitExtension(Utility.GetFileBaseName(documentFullPath))[0];

					// Find the files with the same base name.
					StringCollection matchingFilePaths = new StringCollection();
					foreach(string fullPath in SolutionFiles)
					{
						string[] filePathSegments = Utility.SplitExtension(fullPath);
						if(!validFileExtensions.Contains(filePathSegments[1]))
						{
							// Disregard any files that don't match our expected extensions.
							continue;
						}

						string baseName = Utility.GetFileBaseName(filePathSegments[0]);
						if(string.Compare(documentBaseName, baseName, true) == 0)
						{
							matchingFilePaths.Add(fullPath);
						}
					}

					// Make sure that we have more than one of them open before we try to switch.
					if(matchingFilePaths.Count > 1)
					{
						int iterationIndex = 0;
						int documentIndex = -1;

						// Figure out which document in the list (if any) is the current document.
						foreach(string matchingFilePath in matchingFilePaths)
						{
							if(string.Compare(documentFullPath, matchingFilePath, true) == 0)
							{
								documentIndex = iterationIndex;
								break;
							}

							++iterationIndex;
						}

						// Make sure we found this document.
						if(documentIndex >= 0)
						{
							// Figure out which document comes next in the sequence.
							int nextDocument = documentIndex + 1;
							if(nextDocument >= matchingFilePaths.Count)
							{
								nextDocument = 0;
							}

							// Open and display the next document.
							m_dte.ItemOperations.OpenFile(matchingFilePaths[nextDocument]);
						}
					}
				}
			});
		}

		public RegistryData LoadRegistryData()
		{
			RegistryData data = new RegistryData();

			RegistryKey key = Registry.CurrentUser.OpenSubKey(RegistryData.KeyName);
			if(key == null)
			{
				return data;
			}

			string lastSearchTerm = key.GetValue(RegistryData.ValueName_LastSearchTerm, "") as string;
			string caseSensitive = key.GetValue(RegistryData.ValueName_CaseSensitive, "") as string;

			key.Close();

			bool isCaseSensitive = false;
			bool.TryParse(caseSensitive, out isCaseSensitive);

			data.LastSearchTerm = lastSearchTerm;
			data.IsCaseSensitive = isCaseSensitive;

			return data;
		}

		public void SaveRegistryData(RegistryData data)
		{
			RegistryKey key = Registry.CurrentUser.CreateSubKey(RegistryData.KeyName);

			if(key != null)
			{
				key.SetValue(RegistryData.ValueName_LastSearchTerm, data.LastSearchTerm);
				key.SetValue(RegistryData.ValueName_CaseSensitive, data.IsCaseSensitive.ToString());
				key.Close();
			}
		}

		public void OutputDebugInfo(string msg)
		{
			ThreadHelper.JoinableTaskFactory.Run(async () => {
				await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();
				m_debugInfoWindowPane.OutputString(msg);
			});
		}

		private void RecurseProjectItems(ProjectItems projectItems, ref ICollection<string> filePaths)
		{
			if(projectItems == null)
			{
				return;
			}

			ThreadHelper.ThrowIfNotOnUIThread();

			string[] forbiddenExtensions = Utility.GetForbiddenFileExtensions();

			foreach(ProjectItem item in projectItems)
			{
				if(item.Kind == VSConstants.ItemTypeGuid.PhysicalFile_string)
				{
					for(int i = 0; i < item.FileCount; ++i)
					{
						// All filenames start at index 1 because reasons.
						string filePath = item.FileNames[(short)(i + 1)];
						string fileExtension = Utility.SplitExtension(filePath)[1];

						if(!forbiddenExtensions.Contains(fileExtension))
						{
							// All files with forbidden extensions will be ignored,
							// the rest being added to the output collection.
							filePaths.Add(filePath);
						}
					}
				}

				// Recurse into any sub-items within the current item.
				RecurseProjectItems(item.ProjectItems, ref filePaths);

				// Recurse into sub-projects.
				RecurseProjectItems(item.SubProject?.ProjectItems, ref filePaths);
			}
		}

		#endregion
	}
}
