﻿//------------------------------------------------------------------------------
// <copyright file="SolutionOpenPackage.cs" company="Company">
//	 Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using EnvDTE;
using Microsoft;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Threading;

using Task = System.Threading.Tasks.Task;

namespace SolutionOpen
{
	/// <summary>
	/// This is the class that implements the package exposed by this assembly.
	/// </summary>
	/// <remarks>
	/// <para>
	/// The minimum requirement for a class to be considered a valid package for Visual Studio
	/// is to implement the IVsPackage interface and register itself with the shell.
	/// This package uses the helper classes defined inside the Managed Package Framework (MPF)
	/// to do it: it derives from the Package class that provides the implementation of the
	/// IVsPackage interface and uses the registration attributes defined in the framework to
	/// register itself and its components with the shell. These attributes tell the pkgdef creation
	/// utility what data to put into .pkgdef file.
	/// </para>
	/// <para>
	/// To get loaded into VS, the package must be referred by &lt;Asset Type="Microsoft.VisualStudio.VsPackage" ...&gt; in .vsixmanifest file.
	/// </para>
	/// </remarks>
	[PackageRegistration(UseManagedResourcesOnly = true, AllowsBackgroundLoading = true)]
	[InstalledProductRegistration("#110", "#112", "1.0", IconResourceID = 400)] // Info on this package for Help/About
	[Guid(PackageGuidString)]
	[SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "pkgdef, VS and vsixmanifest are valid VS terms")]
	[ProvideMenuResource("Menus.ctmenu", 1)]
	[ProvideAutoLoad(VSConstants.UICONTEXT.SolutionExistsAndFullyLoaded_string, PackageAutoLoadFlags.BackgroundLoad)]
	public sealed class Package : AsyncPackage, IVsSolutionEvents, IVsHierarchyEvents, IVsUpdateSolutionEvents
	{
		/// <summary>
		/// SolutionOpenPackage GUID string.
		/// </summary>
		public const string PackageGuidString = "e56d7fed-59bc-4323-b20f-17df4f10dbec";

		/// <summary>
		/// Initializes a new instance of the <see cref="Package"/> class.
		/// </summary>
		public Package()
		{
			// Inside this method you can place any initialization code that does not require
			// any Visual Studio service because at this point the package object is created but
			// not sited yet inside Visual Studio environment. The place to do all the other
			// initialization is the InitializeAsync method.
		}

		#region AsyncPackage Implementation

		protected override async Task InitializeAsync(CancellationToken cancellationToken, IProgress<ServiceProgressData> progress)
		{
			await JoinableTaskFactory.SwitchToMainThreadAsync();

			DTE dte = await GetServiceAsync(typeof(SDTE)) as DTE;
			Assumes.Present(dte);

			m_outputWindow = await GetServiceAsync(typeof(SVsOutputWindow)) as IVsOutputWindow;
			Assumes.Present(m_outputWindow);

			m_outputWindow.GetPane(VSConstants.OutputWindowPaneGuid.BuildOutputPane_guid, out m_buildWindowPane);
			Assumes.Present(m_buildWindowPane);

			m_outputWindow.CreatePane(DebugInfoWindowPaneGuid, "Solution Open", 0, 0);
			m_outputWindow.GetPane(DebugInfoWindowPaneGuid, out m_debugInfoWindowPane);
			Assumes.Present(m_debugInfoWindowPane);

			m_solution = await GetServiceAsync(typeof(SVsSolution)) as IVsSolution;
			Assumes.Present(m_solution);

			m_buildManager = await GetServiceAsync(typeof(SVsSolutionBuildManager)) as IVsSolutionBuildManager;
			Assumes.Present(m_buildManager);

			Command.FileSearchCommand.Initialize(this);
			Command.HeaderFlipCommand.Initialize(this);

			ErrorHandler.ThrowOnFailure(m_solution.AdviseSolutionEvents(this, out m_solutionEventsCookie));
			ErrorHandler.ThrowOnFailure(m_buildManager.AdviseUpdateSolutionEvents(this, out m_buildEventsCookie));

			new PackageData(dte, m_debugInfoWindowPane);
		}

		#endregion
		#region Static Members

		private static readonly Guid DebugInfoWindowPaneGuid = new Guid("8909C32D-9F95-4C75-80DA-FCC9B663AC47");

		#endregion
		#region instance Members

		private Dictionary<IVsHierarchy, uint> hierarchyMap = new Dictionary<IVsHierarchy, uint>();

		private uint m_solutionEventsCookie;
		private uint m_buildEventsCookie;

		private IVsSolution m_solution;
		private IVsSolutionBuildManager m_buildManager;
		private IVsOutputWindow m_outputWindow;
		private IVsOutputWindowPane m_buildWindowPane;
		private IVsOutputWindowPane m_debugInfoWindowPane;
		private DateTime m_startTime;

		#endregion
		#region IVsSolutionEvents implementation

		public int OnAfterOpenProject(IVsHierarchy hierarchy, int added)
		{
			int hr = VSConstants.S_OK;
			uint cookie = 0;

			JoinableTaskFactory.Run(async () => {
				await JoinableTaskFactory.SwitchToMainThreadAsync();
				hr = hierarchy.AdviseHierarchyEvents(this, out cookie);
			});

			if(hr == VSConstants.S_OK)
			{
				hierarchyMap.Add(hierarchy, cookie);
			}

			return hr;
		}

		public int OnQueryCloseProject(IVsHierarchy hierarchy, int removing, ref int cancel)
		{
			return VSConstants.S_OK;
		}

		public int OnBeforeCloseProject(IVsHierarchy hierarchy, int removed)
		{
			int hr = VSConstants.S_OK;
			uint cookie = 0;

			if(hierarchyMap.TryGetValue(hierarchy, out cookie))
			{
				hierarchyMap.Remove(hierarchy);

				JoinableTaskFactory.Run(async () => {
					await JoinableTaskFactory.SwitchToMainThreadAsync();
					hr = hierarchy.UnadviseHierarchyEvents(cookie);
				});
			}

			return hr;
		}

		public int OnAfterLoadProject(IVsHierarchy stubHierarchy, IVsHierarchy realHierarchy)
		{
			PackageData.Instance.MakeDirty();
			return VSConstants.S_OK;
		}

		public int OnQueryUnloadProject(IVsHierarchy realHierarchy, ref int cancel)
		{
			return VSConstants.S_OK;
		}

		public int OnBeforeUnloadProject(IVsHierarchy realHierarchy, IVsHierarchy stubHierarchy)
		{
			return VSConstants.S_OK;
		}

		public int OnAfterOpenSolution(object unkReserved, int newSolution)
		{
			PackageData.Instance.MakeDirty();
			return VSConstants.S_OK;
		}

		public int OnQueryCloseSolution(object unkReserved, ref int cancel)
		{
			return VSConstants.S_OK;
		}

		public int OnBeforeCloseSolution(object unkReserved)
		{
			JoinableTaskFactory.Run(async () => {
				await JoinableTaskFactory.SwitchToMainThreadAsync();

				m_buildManager.UnadviseUpdateSolutionEvents(m_buildEventsCookie);
				m_solution.UnadviseSolutionEvents(m_solutionEventsCookie);


			});

			return VSConstants.S_OK;
		}

		public int OnAfterCloseSolution(object unkReserved)
		{
			return VSConstants.S_OK;
		}

		#endregion
		#region IVsHierarchyEvents implementation

		public int OnItemAdded(uint itemIdParent, uint itemIdSiblingPrev, uint itemIdAdded)
		{
			PackageData.Instance.MakeDirty();
			return VSConstants.S_OK;
		}

		public int OnItemsAppended(uint itemIdParent)
		{
			PackageData.Instance.MakeDirty();
			return VSConstants.S_OK;
		}

		public int OnItemDeleted(uint itemId)
		{
			PackageData.Instance.MakeDirty();
			return VSConstants.S_OK;
		}

		public int OnPropertyChanged(uint itemId, int propId, uint flags)
		{
			return VSConstants.S_OK;
		}

		public int OnInvalidateItems(uint itemIdParent)
		{
			PackageData.Instance.MakeDirty();
			return VSConstants.S_OK;
		}

		public int OnInvalidateIcon(IntPtr icon)
		{
			return VSConstants.S_OK;
		}

		#endregion
		#region IVsUpdateSolutionEvents implementation

		public int UpdateSolution_Begin(ref int cancelUpdate)
		{
			return VSConstants.S_OK;
		}

		public int UpdateSolution_Done(int succeeded, int modified, int cancelCommand)
		{
			DateTime now = DateTime.Now;
			TimeSpan timeSpan = now - m_startTime;

			JoinableTaskFactory.Run(async () => {
				await JoinableTaskFactory.SwitchToMainThreadAsync();

				m_buildWindowPane.OutputString(string.Format("Total build time: {0:00}s | {1:00}:{2:00}:{3:00}.{4:00}\n", new object[]
				{
					timeSpan.TotalSeconds,
					timeSpan.Hours,
					timeSpan.Minutes,
					timeSpan.Seconds,
					timeSpan.Milliseconds / 10
				}));
			});

			return VSConstants.S_OK;
		}

		public int UpdateSolution_StartUpdate(ref int cancelUpdate)
		{
			m_startTime = DateTime.Now;
			return VSConstants.S_OK;
		}

		public int UpdateSolution_Cancel()
		{
			return VSConstants.S_OK;
		}

		public int OnActiveProjectCfgChange(IVsHierarchy hierarchy)
		{
			return VSConstants.S_OK;
		}

		#endregion
	}
}
