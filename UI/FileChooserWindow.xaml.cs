﻿using Microsoft.VisualStudio.Utilities;
using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SolutionOpen.UI
{
	public class FilePath
	{
		public FilePath(string fileName, string fullFilePath)
		{
			FileName = fileName;
			FullFilePath = fullFilePath;
		}

		public string FileName { get; }

		public string FullFilePath { get; }
	};

	/// <summary>
	/// Interaction logic for FileChooserWindow.xaml
	/// </summary>
	public partial class FileChooserWindow : Window
	{
		public FileChooserWindow()
		{
			InitializeComponent();

			DataContext = this;

			m_registryData = PackageData.Instance.LoadRegistryData();
			m_listSortHandler = new GridViewSortHandler(DefaultSortingColumn);
			m_arrowUpTemplate = Resources["HeaderTemplateArrowUp"] as DataTemplate;
			m_arrowDownTemplate = Resources["HeaderTemplateArrowDown"] as DataTemplate;

			// Fill the collection with the file path objects.
			foreach(string path in PackageData.Instance.SolutionFiles)
			{
				string filename = Utility.GetFileBaseName(path);

				FilePaths.Add(new FilePath(filename, path));
			}

			ICollectionView collectionView = GetDataViewCollection();

			collectionView.Filter = new Predicate<object>((item) => {
				string filename = (item as FilePath)?.FileName;

				if(filename == null)
				{
					return false;
				}

				if(!CaseSensitive_Checkbox.IsChecked.Value)
				{
					filename = filename.ToLower();
				}

				if(m_filterRegex == null || m_filterRegex.IsMatch(filename))
				{
					return true;
				}

				return false;
			});

			// Give the list an initial sorting.  This along with passing the default sorting column header
			// to the sort handler is a hack because if we attempt to do a proper sort on that column header,
			// we'd find out that its column member is null, despite having been initialized by this point.
			// This results in a null access exception no matter what and this is the best way I have found
			// to get around it.  It's not ideal, but it works.
			collectionView.SortDescriptions.Clear();
			collectionView.SortDescriptions.Add(new SortDescription("FileName", ListSortDirection.Ascending));

			FilenameTextBox.Text = m_registryData.LastSearchTerm;
			CaseSensitive_Checkbox.IsChecked = m_registryData.IsCaseSensitive;

			// Set the initial focus on the filename box.
			FilenameTextBox.Focus();
			FilenameTextBox.Select(FilenameTextBox.Text.Length, 0);
		}

		#region Instance Members

		public ObservableCollection<FilePath> FilePaths { get; } = new ObservableCollection<FilePath>();

		private RegistryData m_registryData;
		private Regex m_filterRegex;
		private GridViewSortHandler m_listSortHandler;
		private DataTemplate m_arrowUpTemplate;
		private DataTemplate m_arrowDownTemplate;

		#endregion
		#region Methods

		private ICollectionView GetDataViewCollection()
		{
			return CollectionViewSource.GetDefaultView(FileListView.ItemsSource ?? FileListView.Items);
		}

		private void BuildFilterRegex()
		{
			ICollectionView collectionView = GetDataViewCollection();
			string pattern = FilenameTextBox.Text;

			if(pattern.Length == 0)
			{
				m_filterRegex = null;

				collectionView.Refresh();
			}
			else
			{
				try
				{
					Regex regex = new Regex(CaseSensitive_Checkbox.IsChecked.Value ? pattern : pattern.ToLower());

					m_filterRegex = regex;

					collectionView.Refresh();
				}
				catch
				{
					// Do nothing when the regex fails to compile.
				}
			}
		}

		#endregion
		#region Events

		private void GridViewColumnHeader_Click(object sender, RoutedEventArgs e)
		{
			GridViewColumnHeader clickedHeader = e.OriginalSource as GridViewColumnHeader;
			if(clickedHeader != null)
			{
				m_listSortHandler.Sort(GetDataViewCollection(), clickedHeader, m_arrowUpTemplate, m_arrowDownTemplate);
			}
		}

		private void OpenButton_Click(object sender, RoutedEventArgs e)
		{
			IList selectedItems = FileListView.SelectedItems;

			if(selectedItems.Count > 0)
			{
				IList<string> filePaths = new List<string>(selectedItems.Count);

				foreach(object item in selectedItems)
				{
					FilePath pathObj = item as FilePath;
					if(pathObj != null)
					{
						filePaths.Add(pathObj.FullFilePath);
					}
				}

				PackageData.Instance.OpenFiles(filePaths.ToArray());
			}

			Close();
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void FilenameTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			BuildFilterRegex();

			m_registryData.LastSearchTerm = FilenameTextBox.Text;
		}

		private void CaseSensitiveCheckBox_Toggled(object sender, RoutedEventArgs e)
		{
			BuildFilterRegex();

			m_registryData.IsCaseSensitive = CaseSensitive_Checkbox.IsChecked.Value;
		}

		private void FileListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			OpenButton.IsEnabled = FileListView.SelectedItems.Count > 0;
		}

		private void Window_Closed(object sender, EventArgs e)
		{
			PackageData.Instance.SaveRegistryData(m_registryData);
		}

		#endregion

		private void FilenameTextBox_KeyUp(object sender, KeyEventArgs e)
		{
			// When the 'down' key is pressed, switch focus to the file list.
			if(e.Key == Key.Down)
			{
				ICollectionView collectionView = GetDataViewCollection();
				bool needsItemSelection = (FileListView.SelectedItem == null) && !collectionView.IsEmpty;

				if(needsItemSelection)
				{
					FileListView.SelectedIndex = 0;
				}

				FileListView.ScrollIntoView(FileListView.SelectedItem);
				FileListView.Focus();

				var item = FileListView.ItemContainerGenerator.ContainerFromIndex(FileListView.SelectedIndex);

				Keyboard.Focus(item as ListViewItem);
			}
		}
	}
}
