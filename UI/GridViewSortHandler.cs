﻿using Microsoft;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace SolutionOpen.UI
{
	class GridViewSortHandler
	{
		public GridViewSortHandler(GridViewColumnHeader initialHeader)
		{
			m_lastHeaderClicked = initialHeader;
		}

		#region Instance Members

		private GridViewColumnHeader m_lastHeaderClicked = null;
		private ListSortDirection m_lastDirection = ListSortDirection.Ascending;

		#endregion
		#region Methods

		public void Sort(ICollectionView collectionView, GridViewColumnHeader clickedHeader, DataTemplate arrowUpTemplate, DataTemplate arrowDownTemplate)
		{
			ListSortDirection newDirection;

			if(clickedHeader != null)
			{
				if(clickedHeader.Role != GridViewColumnHeaderRole.Padding)
				{
					if(clickedHeader != m_lastHeaderClicked)
					{
						if(clickedHeader.Column.HeaderTemplate == arrowUpTemplate)
						{
							newDirection = ListSortDirection.Descending;
						}
						else
						{
							newDirection = ListSortDirection.Ascending;
						}
					}
					else
					{
						if(m_lastDirection == ListSortDirection.Ascending)
						{
							newDirection = ListSortDirection.Descending;
						}
						else
						{
							newDirection = ListSortDirection.Ascending;
						}
					}

					string propertyName = ((Binding) clickedHeader.Column.DisplayMemberBinding).Path.Path;

					SortDescription sortDesc = new SortDescription(propertyName, newDirection);

					collectionView.SortDescriptions.Clear();
					collectionView.SortDescriptions.Add(sortDesc);
					collectionView.Refresh();

					if(m_lastHeaderClicked != null)
					{
						m_lastHeaderClicked.Column.HeaderTemplate = null;
					}

					clickedHeader.Column.HeaderTemplate = (newDirection == ListSortDirection.Ascending) ? arrowUpTemplate : arrowDownTemplate;

					m_lastHeaderClicked = clickedHeader;
					m_lastDirection = newDirection;
				}
			}
		}

		#endregion
	}
}
