﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolutionOpen
{
	public class RegistryData
	{
		public RegistryData()
		{
		}

		public static readonly string KeyName = @"Software\JeffGrills\SolutionOpen";
		public static readonly string ValueName_LastSearchTerm = "LastSearch";
		public static readonly string ValueName_CaseSensitive = "CaseSensitive";

		public string LastSearchTerm { get; set; } = string.Empty;

		public bool IsCaseSensitive { get; set; }
	}
}
